package wicondev.ui.text.play

import play.api.data.Field
import play.api.mvc.Call

import wicondev.ui.generic.sui.{SuiClassSet, classes}
import wicondev.ui.text.sui.all._

trait Form {
  object Form {
    def apply(
      route: Call,
      formClasses: String*
    )(content: Frag*): Frag = makeForm(
      route,
      s.ui.form.withClasses(formClasses: _*)
    )(content: _*)

    def large(
      route: Call,
      formClasses: String*
    )(content: Frag*): Frag = makeForm(
      route,
      s.ui.large.form.withClasses(formClasses: _*)
    )(content: _*)

    def small(
      route: Call,
      formClasses: String*
    )(content: Frag*): Frag = makeForm(
      route,
      s.ui.small.form.withClasses(formClasses: _*)
    )(content: _*)

    private def makeForm(
      route: Call,
      formClasses: SuiClassSet
    )(content: Frag*): Frag = {
      <.form(
        formClasses,
        ^.role   := "form",
        ^.method := route.method,
        ^.action := route.url
      )(content)
    }

  }

  object Input{
    def Text(
      field:      Field,
      fieldLabel: String,
      attributes: Modifier*
    ): Frag =
      input(
        field     = field,
        fieldType = InputType.TEXT
      )(attributes)

    def WrappedText(
      field:      Field,
      fieldLabel: String,
      attributes: Modifier*
    ): Frag =
      wrappedInput(
        field        = field,
        fieldLabel   = fieldLabel,
        fieldType = InputType.TEXT
      )(attributes)

    def Email(
      field: Field,
      fieldLabel: String,
      attributes: Modifier*
    ): Frag =
      wrappedInput(
        field      = field,
        fieldLabel = fieldLabel,
        fieldType = InputType.EMAIL
      )(attributes)

    def Password(
      field: Field,
      fieldLabel: String,
      attributes: Modifier*
    ): Frag =
      wrappedInput(
        field      = field,
        fieldLabel = fieldLabel,
        fieldType = InputType.PASSWORD
      )(attributes)

    def Number(
      field: Field,
      fieldLabel: String,
      attributes: Modifier*
    ): Frag =
      wrappedInput(
        field      = field,
        fieldLabel = fieldLabel,
        fieldType = InputType.NUMBER
      )(attributes)

    def Checkbox(
      field: Field,
      fieldLabel: String,
      attributes: Modifier*
    ): Frag =
      <.div(s.ui.checkbox)(
        input(
          field     = field,
          fieldType = InputType.CHECK_BOX,
          fieldTag  = <.input
        )(attributes),
        <.label(
          ^.`for` := field.id
        )(fieldLabel)
      )

    def TextArea(
      field: Field,
      attributes: Modifier*
    ): Frag =
      <.div(classSet1(
        classes.field,
        classes.error -> field.hasErrors
      ))(
        <.textarea(
          ^.id   := field.id,
          ^.name := field.name,
          attributes
        )(raw(field.value getOrElse ""))
      )

    private def wrappedInput(
      field:        Field,
      fieldLabel:   String,
      fieldType:    InputType,
      fieldTag:     ConcreteHtmlTag[String] = <.input
    )(attributes: Modifier*): Frag =
      <.div(classSet1(
        classes.field,
        classes.error -> field.hasErrors
      ))(
        <.label(fieldLabel),
        input(
          field        = field,
          fieldType    = fieldType,
          fieldTag     = fieldTag
        )(attributes)
      )

    private def input(
      field:        Field,
      fieldType:    InputType,
      fieldTag:     ConcreteHtmlTag[String] = <.input
    )(attributes:   Modifier*): Frag =
      fieldTag(
        ^.tpe   := fieldType.asString,
        ^.id    := field.id,
        ^.name  := field.name,
        if(fieldType != InputType.PASSWORD) ^.value := field.value getOrElse ""
        else ^.value := "",
        attributes
      )
  }
}
