package wicondev.ui.text.play

import play.api.i18n.Messages
import play.api.mvc.RequestHeader

import wicondev.ui.generic.TypedClass
import wicondev.ui.text.sui.all._

trait Flash {
  object Flash {
    def apply(
      key: String,
      messageType: MessageType = MessageType.INFO
    )(implicit request:  RequestHeader, messages: Messages): Frag =
      request.flash.get(key).map { message: String =>
        Alert(messageType)(Messages(message))
      }.getOrElse("")

    def apply(
      flashType: FlashType
    )(implicit request:  RequestHeader, messages: Messages): Frag = flashType match {
      case FlashType.SUCCESS => apply(flashType.asString, MessageType.SUCCESS)
      case FlashType.INFO    => apply(flashType.asString, MessageType.INFO)
      case FlashType.WARNING => apply(flashType.asString, MessageType.WARNING)
      case FlashType.ERROR   => apply(flashType.asString, MessageType.ERROR)
    }

    def success(implicit request: RequestHeader, messages: Messages): Frag = apply(FlashType.SUCCESS)
    def info   (implicit request: RequestHeader, messages: Messages): Frag = apply(FlashType.INFO)
    def warning(implicit request: RequestHeader, messages: Messages): Frag = apply(FlashType.WARNING)
    def error  (implicit request: RequestHeader, messages: Messages): Frag = apply(FlashType.ERROR)
    def all    (implicit request: RequestHeader, messages: Messages): Seq[Frag] =
      Seq(
        success,
        info,
        warning,
        error
      )
  }
  
  case class FlashType(value: String) extends TypedClass(value)

  object FlashType {
    val SUCCESS = FlashType("success")
    val INFO    = FlashType("info")
    val WARNING = FlashType("warning")
    val ERROR   = FlashType("error")
  }
}
