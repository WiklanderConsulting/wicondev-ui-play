lazy val root = project.in(file("."))
  .settings(
    name           := Settings.name,
    normalizedName := Settings.normalizedName,
    version        := Settings.version,
    organization   := Settings.organization,
    scalaVersion   := Settings.versions.scala,

    homepage       := Settings.homepage,
    licenses       += Settings.licenses,
    scmInfo        := Settings.scmInfo,

    publishTo <<= version { Settings.publishTo() },
    publishMavenStyle := true,
    publishArtifact in Test := false,

    pomExtra             := Settings.pomExtra,
    pomIncludeRepository := { _ => false },

    libraryDependencies ++= Settings.jvmDependencies.value
  )
  .settings(Settings.resolverSettings: _*)
