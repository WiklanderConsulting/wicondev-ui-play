import sbt._

/**
 * Application settings. Configure the build for your application here.
 * You normally don't have to touch the actual build definition after this.
 */
object Settings extends WicondevSettings("UI Play") {
  /** The version of your application */
  val version = "0.4.0-SNAPSHOT"

  /** Dependencies */
  val jvmDependencies = Def.setting(Seq(
    "se.wicondev"         %% "wicondev-ui-semantic" % "0.4.0-SNAPSHOT",
    "com.typesafe.play"   %% "play"                 % "2.6.7"
  ))
}
